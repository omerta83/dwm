/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned short alpha   = 0xdd;      /* background color alpha */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
// static const char *fonts[]          = { "monospace:size=10" };
static const char *fonts[]          = {
    /* HiDPI */
    //"Terminus (TTF):antialias=true:size=12",
    //"Material Icons:antialias=true:size=12",
    //"Ionicons:antialias=true:size=12",
    /* Non-HiDPI screen */
    "Misc Tamsyn:antialias=true:size=21",
    "Material Icons:antialias=true:size=17",
    "Ionicons:antialias=true:size=17",
};
static const char dmenufont[]       = "Hack:size=12";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
// static const char *colors[][3]      = {
// 	#<{(|               fg         bg         border   |)}>#
// 	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
// 	[SchemeSel] =  { col_gray4, col_cyan,  col_cyan  },
// };
static const char dark[]                = "#282828";
static const char black[]               = "#282828";
static const char gray[]                 = "#373737";
static const char white[]                = "#FFFFFF";
static const char cyan[]                 = "#268bd2";
static const char *colors[][3] = {
	/*               fg         bg         border   */
	[SchemeNorm] = { white, black, black },
	[SchemeSel] =  { cyan, black, cyan },
};

/* tagging */
// static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//                             terminal  browsers  docs      pdf       chat      music     video     steam
// static const char *tags[] = { "\uf120", "\uf269", "\uf121", "\uf212", "\uf2c6", "\uf1bc", "\uf144", "\uf1b6", "\uf108" };
//                             terminal  code      browsers  books     chat      music     video     play
// static const char *tags[] = { "\uf227", "\uf271", "\uf4db", "\uf3e8", "\uf4f0", "\uf20c", "\uf24d", "\uf30c" };
static const char *tags[] = { "\uf227", "\uf271", "\uf4db", "\uf3e8", "\uf4f0", "\uf20c", "\uf24d" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class            instance    title       tags mask     isfloating   monitor */
	{ "xterm-256color", NULL,       NULL,       1,            0,           -1 },
        { "xfce4-terminal", NULL,       NULL,       1,            0,           -1 },
	{ "Termite",	    NULL,	NULL,	    1,		  0,	       -1 },
        { "Emacs",          NULL,       NULL,       2,            0,           -1 },
	{ "jetbrains-studio", NULL,	NULL,	    2,		  0,	       -1 },
	{ "jetbrains-phpstorm", NULL,	NULL,	    2,		  0,	       -1 },
	{ "jetbrains-pycharm", NULL,	NULL,	    2,		  0,	       -1 },
	{ "jetbrains-intellij", NULL,	NULL,	    2,		  0,	       -1 },
	{ "jetbrains-goland", NULL,	NULL,	    2,		  0,	       -1 },
	{ "Google-chrome",  NULL,       NULL,       1 << 2,       0,           -1 },
	{ "qutebrowser",    NULL,       NULL,       1 << 2,       0,           -1 },
	// { "DevDocs",        NULL,       NULL,       1 << 3,       0,           -1 },
	{ "Zathura",        NULL,       NULL,       1 << 3,       0,           -1 },
	{ "Slack-desktop",  NULL,       NULL,       1 << 4,       0,           -1 },
	{ "TelegramDesktop",  NULL,     NULL,       1 << 4,       0,           -1 },
	{ "Spotify",        NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Kodi",           NULL,       NULL,       1 << 6,       0,           -1 },
	{ "mpv",            NULL,       NULL,       1 << 6,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

// static const Layout layouts[] = {
	/* symbol     arrange function */
// 	{ "[\uf0db]",      tile },    #<{(| first entry is default |)}>#
// 	{ "[\uf2d2]",      NULL },    #<{(| no layout function means floating behavior |)}>#
// 	{ "[\uf2d0]",      monocle },
// };
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[F]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-l", "15", "-p", ">", "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *spotifytoggle[] = { "/home/omerta/.local/bin/sp", "play", NULL };
static const char *spotifynext[] = { "/home/omerta/.local/bin/sp", "next", NULL };
static const char *spotifyprev[] = { "/home/omerta/.local/bin/sp", "prev", NULL };
static const char *brightnessup[] = { "gmux_backlight", "+100", NULL };
static const char *brightnessdown[] = { "gmux_backlight", "-100", NULL };
static const char *volumeinc[] = { "amixer", "set", "Master", "5%+", NULL };
static const char *volumedec[] = { "amixer", "set", "Master", "5%-", NULL };
static const char *volumemute[] = { "amixer", "set", "Master", "toggle", NULL };
// static const char *browsercmd[] = { "google-chrome-stable", "--force-device-scale-factor=1", NULL };
// static const char *browsercmd[] = { "qutebrowser", NULL };
static const char *roficmd[] = { "rofi", "-show", "drun", NULL };
//static const char *termcmd[]  = { "urxvt", NULL };
// static const char *termcmd[]  = { "st", NULL };
// static const char *termcmd[] = { "termite", NULL };
static const char *termcmd[] = { "terminator", NULL };
//static const char *qutebrowser[] = { "qutebrowser", NULL, NULL, NULL, "qutebrowser" };
//static const char *chrome[] = { "google-chrome-stable", NULL, NULL, NULL, "Google-chrome" };
static const char *chrome[] = { "google-chrome-stable", "--force-device-scale-factor=1.3", NULL };
//static const char *spotify[] = { "spotify", "--force-device-scale-factor=1.3", NULL };
// static const char *devdocs[] = { "/home/omerta/Applications/DevDocs/devdocs", "--force-device-scale-factor=1.3", NULL};
//static const char *devdocs[] = { "/home/omerta/Applications/DevDocs/devdocs", NULL, NULL, NULL, "DevDocs" };
//static const char *emacs[] = { "emacs", NULL, NULL, NULL, "emacs" };

#include "movestack.c"
#include "maximize.c"
#include "shiftview.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_space,  spawn,          {.v = roficmd } },
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
        //{ MODKEY,                       XK_c,      runorraise,          {.v = qutebrowser } },
        { MODKEY,                       XK_g,      runorraise,          {.v = chrome } },
        // { MODKEY|ShiftMask,             XK_d,      runorraise,          {.v = devdocs } },
        //{ MODKEY,                       XK_e,      runorraise,     {.v = emacs } },
        { 0,                            0x1008ff14,     spawn,          {.v = spotifytoggle} },
        { 0,                            0x1008ff17,     spawn,          {.v = spotifynext} },
        { 0,                            0x1008ff16,     spawn,          {.v = spotifyprev} },
        { 0,                            0x1008ff13,     spawn,          {.v = volumeinc} },
        { 0,                            0x1008ff11,     spawn,          {.v = volumedec} },
        { 0,                            0x1008ff12,     spawn,          {.v = volumemute} },
        { 0,                            0x1008ff03,     spawn,          {.v = brightnessdown} },
        { 0,                            0x1008ff02,     spawn,          {.v = brightnessup} },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
        { MODKEY|ControlMask|ShiftMask, XK_h,      togglehorizontalmax, { 0 } },
        { MODKEY|ControlMask|ShiftMask, XK_l,      togglehorizontalmax, { 0 } },
        { MODKEY|ControlMask|ShiftMask, XK_j,      toggleverticalmax,   { 0 } },
        { MODKEY|ControlMask|ShiftMask, XK_k,      toggleverticalmax,   { 0 } },
        { MODKEY|ControlMask,           XK_m,      togglemaximize,      { 0 } },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	//{ MODKEY,                       XK_Tab,    view,           {0} },
        { MODKEY,                       XK_Tab,    shiftview,      {.i = +1 } },
        //{ MODKEY|ShiftMask,             XK_grave,    shiftview,      {.i = -1 } },
        { MODKEY|ShiftMask,             XK_Tab,    shiftview,      {.i = -1 } },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,      setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

